//
// ex01.hpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d15/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Wed Jan 22 15:32:09 2014 Jean Gravier
// Last update Wed Jan 22 15:34:04 2014 Jean Gravier
//

#ifndef _EX01_H_
#define _EX01_H_

template <typename T>

int	compare(T const& a, T const& b)
{
  if (a == b)
    return (0);
  else if (a < b)
    return (-1);
  else
    return (1);
}

#endif /* _EX01_H_ */
