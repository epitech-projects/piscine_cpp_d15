//
// ex05.hpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d15/ex05
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Wed Jan 22 18:04:33 2014 Jean Gravier
// Last update Wed Jan 22 19:43:58 2014 Jean Gravier
//

#ifndef _EX05_H_
#define _EX05_H_

#include <exception>

template <typename T>

class		array
{
public:
  array();
  array(unsigned int);
  array(array const&);
  ~array();

public:
  unsigned int	size() const;
  T		*getData() const;
  array		&operator=(array const&);
  T		&operator[](int);
  template <typename U>
  array<U>	convertTo(U (*func)(T const&))
  {
    U		tab[this->_size];

    for (int i = 0; i < this->_size; ++i)
      {
	tab[i] = func(this->_data[i]);
      }
    return (*tab);
  }
  void		dump() const;

private:
  int		_size;
  T		*_data;

};

template <typename T>

array<T>::array()
{
  this->_data = new T;
}

template <typename T>

array<T>::array(unsigned int size)
{
  this->_data = new T[size];
}

template <typename T>

array<T>::array(array<T> const& obj)
{
  this->_data = obj.getData();
  this->_size = obj.size();
}

template <typename T>

array<T>::~array()
{
  delete[] this->_data;
}

template <typename T>

array<T>	&array<T>::operator=(array const& obj)
{
  this->_data = obj.getData();
  this->_size = obj.size();
  return (*this);
}

template <typename T>

T		*array<T>::getData() const
{
  return (this->_data);
}

template <typename T>

unsigned int	array<T>::size() const
{
  return (this->_size);
}

template <typename T>

T		&array<T>::operator[](int index)
{
  if ((index >= this->_size) || index < 0)
    throw new std::exception;
  return (this->_data[index]);
}

template <typename T>

void		array<T>::dump() const
{
  int		i;

  std::cout << "[";
  for (i = 0; i < (this->_size - 1); ++i)
    {
      std::cout << this->_data[i] << std::cout << ", ";
    }
  std::cout << this->_data[i] << "]" << std::endl;
}

#endif /* _EX05_H_ */
