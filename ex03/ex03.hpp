//
// ex03.hpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d15/ex03
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Wed Jan 22 16:45:35 2014 Jean Gravier
// Last update Wed Jan 22 17:27:11 2014 Jean Gravier
//

#ifndef _EX03_H_
#define _EX03_H_

#include <iostream>

template <typename T>

void	foreach(T array[], void (*func)(T const&), int size)
{
  for (int i = 0; i < size; ++i)
    {
      func(array[i]);
    }
}

template <typename E>

void	print(E const& element)
{
  std::cout << element << std::endl;
}

#endif /* _EX03_H_ */
