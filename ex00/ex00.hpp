//
// ex00.hpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d15/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Wed Jan 22 15:05:54 2014 Jean Gravier
// Last update Wed Jan 22 15:25:23 2014 Jean Gravier
//

#ifndef _EX00_H_
#define _EX00_H_

template <typename T>

void	swap(T &a, T &b)
{
  T	temp;

  temp = a;
  a = b;
  b = temp;
}

template <typename T>

T	min(T const& a, T const& b)
{
  if (a >= b)
    return (b);
  else
    return (a);
}

template <typename T>

T	max(T const& a, T const& b)
{
  if (a > b)
    return (a);
  else
    return (b);
}

template <typename T>

T	add(T const& a, T const& b)
{
  T result;

  result = a + b;
  return (result);
}

#endif /* _EX00_H_ */
