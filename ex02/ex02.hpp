//
// ex02.hpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d15/ex02
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Wed Jan 22 15:41:11 2014 Jean Gravier
// Last update Wed Jan 22 18:43:45 2014 Jean Gravier
//

#ifndef _EX02_H_
#define _EX02_H_

#include <iostream>

template <typename T>

T	min(T const& a, T const& b)
{
  std::cout << "template min" << std::endl;

  if (a <= b)
    return (a);
  else
    return (b);
}

int	min(int a, int b)
{
  std::cout << "non-template min" << std::endl;
  if (a <= b)
    return (a);
  else
    return (b);
}

template <typename T>

T	templateMin(T a[], int size)
{
  T	mini;

  mini = a[0];
  for (int i = 1; i < size; ++i)
    {
      mini = min<T>(mini, a[i]);
    }
  return (mini);
}

int	nonTemplateMin(int *a, int b)
{
  int	mini;

  mini = a[0];
  for (int i = 1; i < b; ++i)
    {
      mini = min(mini, a[i]);
    }
  return (mini);
}

#endif /* _EX02_H_ */
