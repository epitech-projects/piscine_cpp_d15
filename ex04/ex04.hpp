//
// ex04.hpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d15/ex04
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Wed Jan 22 17:35:19 2014 Jean Gravier
// Last update Wed Jan 22 22:16:12 2014 Jean Gravier
//

#ifndef _EX04_H_
#define _EX04_H_

#include <string>

template <typename T>
bool	equal(T const& a, T const& b);


template <class T>
class	Tester
{
public:
  bool	equal(T const&, T const&) const;
};

#endif /* _EX04_H_ */
