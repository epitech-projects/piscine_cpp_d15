//
// ex04.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d15/ex04
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Wed Jan 22 17:38:53 2014 Jean Gravier
// Last update Wed Jan 22 22:21:40 2014 Jean Gravier
//

#include "ex04.hpp"
#include <string>

template <typename T>
bool	equal(T const& a, T const& b)
{
  return (a == b);
}

template <class T>
bool	Tester<T>::equal(T const& a, T const& b) const
{
  return (a == b);
}

template bool equal<int>(int const& a, int const& b);
template bool equal<float>(float const& a, float const& b);
template bool equal<double>(double const& a, double const& b);
template bool equal<std::string>(std::string const& a, std::string const& b);

template bool Tester<int>::equal(int const& a, int const& b) const;
template bool Tester<float>::equal(float const& a, float const& b) const;
template bool Tester<double>::equal(double const& a, double const& b) const;
template bool Tester<std::string>::equal(std::string const& a, std::string const& b) const;
